package main

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

type YoutubeData struct {
	id          string
	videoTitle  string
	channelName string
	description string
	releaseDate time.Time
	tags        []string
}

type NfoFile struct {
	XMLName   xml.Name    `xml:"episodedetails"`
	Title     string      `xml:"title"`
	Show      string      `xml:"showtitle"`
	Plot      string      `xml:"plot"`
	Id        NfoUniqueId `xml:"uniqueid"`
	Season    string      `xml:"season"`
	Premiered string      `xml:"premiered"`
}

type NfoUniqueId struct {
	XMLName xml.Name `xml:"uniqueid"`
	Id      string   `xml:",chardata"`
	Type    string   `xml:"type,attr"`
	Default bool     `xml:"default,attr"`
}

func main() {
	// TODO: Argument for directory
	const directory = "/data"

	for _, file := range getFiles(directory) {
		var jsonData YoutubeData = readJson(directory + "/" + file + ".info.json")

		if jsonData.id != "" {
			writeNfo(jsonData, directory+"/"+file+".nfo")
		}
	}
}

func getFiles(directory string) []string {
	items, err := ioutil.ReadDir(directory)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	files := make([]string, 0)

	for _, item := range items {
		if filepath.Ext(item.Name()) == ".json" {
			files = append(files, strings.TrimSuffix(item.Name(), ".info.json"))
		}
	}

	return files
}

func readJson(filename string) YoutubeData {
	var output YoutubeData

	jsonFile, err := os.Open(filename)
	if err != nil {
		fmt.Println(err)
		return output
	}
	// fmt.Println(filename + " opened")

	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var result map[string]interface{}
	json.Unmarshal([]byte(byteValue), &result)

	uploadtime, _ := time.Parse("20060102", result["upload_date"].(string))

	output = YoutubeData{
		id:          result["id"].(string),
		videoTitle:  result["fulltitle"].(string),
		channelName: result["channel"].(string),
		description: filterDescription(result["description"].(string)),
		releaseDate: uploadtime,
		// TODO
		// tags:        result["tags"].([]interface{}),
	}

	if output.description == "" {
		fmt.Println(output)
	}

	return output
}

func writeNfo(youtubeData YoutubeData, outfile string) bool {
	newNfo := NfoFile{
		Title:     youtubeData.videoTitle,
		Show:      youtubeData.channelName,
		Plot:      youtubeData.description,
		Season:    youtubeData.releaseDate.Format("2006"),
		Premiered: youtubeData.releaseDate.Format("2006-01-02"),
	}

	newNfo.Id = NfoUniqueId{
		Id:      youtubeData.id,
		Type:    "youtube",
		Default: true,
	}

	var writer bytes.Buffer

	// TODO: reuse this encoder
	enc := xml.NewEncoder(&writer)
	enc.Indent("  ", "    ")
	enc.Encode(newNfo)

	err := ioutil.WriteFile(outfile, []byte("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"+writer.String()), 0644)

	println(err)

	return false
}

// TODO: No idea how I'm going to make this general. Also just a mess of different youtube description and way too much regex
func filterDescription(description string) string {
	var output string = ""

	/*
	   TODO: This function contains a metric ton of reused regex that is compiled each time. Haven't looked into Go enough for how best
	       to reuse regex without a ton of global vars. Maybe the Go is smart enough to handle it at compile-time?
	*/

	regex := regexp.MustCompile(`(?ms)www\.facebook\.com\/[tT]heyre[jJ]ust[mM]ovies\/?(.+)GET\sMERCH`)
	result := regex.FindStringSubmatch(description)

	if len(result) > 1 {
		output = strings.TrimSpace(result[1])
	}

	if len(output) < 1 {
		// Older when podcast was called "Carpool critics"
		regex := regexp.MustCompile(`(?ms)carpoolcriticspodcast\@gmail.com(.+)GET\sMERCH`)
		result := regex.FindStringSubmatch(description)

		if len(result) > 1 {
			output = strings.TrimSpace(result[1])
		}
	}

	if len(output) < 1 {
		// Older when podcast was called "Carpool critics" (pre-covid)
		regex := regexp.MustCompile(`(?ms)www\.facebook\.com\/[Cc]arpool[cC]ritics\/?(.+)GET\sMERCH`)
		result := regex.FindStringSubmatch(description)

		if len(result) > 1 {
			output = strings.TrimSpace(result[1])
		}
	}

	if len(output) < 1 {
		// Even older audio talkshow style
		regex := regexp.MustCompile(`(?ms)(The\s+\'?[Cc]ritics discuss.+)GET\sMERCH`)
		result := regex.FindStringSubmatch(description)

		if len(result) > 1 {
			output = strings.TrimSpace(result[1])
		}
	}

	if len(output) < 1 {
		// First episodes had no structure
		if regexp.MustCompile(`CarpoolCritics`).MatchString(description) {
			output = description

			output = regexp.MustCompile(`(?m)^Twitter\:.*$`).ReplaceAllString(output, ``)
			output = regexp.MustCompile(`(?m)^Email\:.*$`).ReplaceAllString(output, ``)
			output = regexp.MustCompile(`(?m)^Facebook\:.*$`).ReplaceAllString(output, ``)
			output = regexp.MustCompile(`(?m)^\s*\-\-\-+\s*$`).ReplaceAllString(output, ``)
			output = regexp.MustCompile(`(?m)^LTX\s+EXPO\:.*$`).ReplaceAllString(output, ``)
			output = regexp.MustCompile(`(?m)^SUPPORT\s+US\s+ON\s+FLOATPLANE\:.*$`).ReplaceAllString(output, ``)

			output = regexp.MustCompile(`(?m)^.*Manscaped\.com.*$`).ReplaceAllString(output, ``)
			output = regexp.MustCompile(`(?m)^.*https?:\/\/lmg\.gg\/\w+.*$`).ReplaceAllString(output, ``)
			output = regexp.MustCompile(`(?m)^.*carpoolcritics\.libsyn\.com.*$`).ReplaceAllString(output, ``)
			output = regexp.MustCompile(`(?m)^.*www\.LTTStore\.com.*$`).ReplaceAllString(output, ``)
			output = regexp.MustCompile(`(?m)Follow\s+us\s+on\s+Twitter\s+\@CarpoolCritics\s*\!\!.*`).ReplaceAllString(output, ``)
			output = regexp.MustCompile(`(?m)Follow\s+us\s+on\s+Twitter https?:\/\/twitter.com/CarpoolCritics/?.*$`).ReplaceAllString(output, ``)

			// Plain strings to find too
			// output = strings.Replace(output, "", ``, -1)
			// output = strings.Replace(output, "", ``, -1)
			output = strings.Replace(output, "Follow @CarpoolCritics on Twitter and tell us what you think!", ``, -1)
			output = strings.Replace(output, "FOLLOW US ON SOCIAL", ``, -1)
			output = strings.Replace(output, "FOLLOW OUR OTHER CHANNELS", ``, -1)
			output = strings.Replace(output, "AFFILIATES & REFERRALS", ``, -1)
			output = strings.Replace(output, "Listen to the Podcast: https://carpoolcritics.ca", ``, -1)
			output = strings.Replace(output, "Follow us @CarpoolCritics on Twitter!", ``, -1)
			output = strings.Replace(output, "Ask us weird questions on Twitter @CarpoolCritics !!", ``, -1)
			output = strings.Replace(output, "Follow us @CarpoolCritics on Twitter!", ``, -1)
		}
	}

	return strings.TrimSpace(output)
}
