FROM golang:alpine as build

COPY youtube-nfo.go /tmp/
RUN cd /tmp/ && GOOS=linux GOARCH=amd64 go build -o /tmp/youtube-nfo /tmp/youtube-nfo.go && chmod +x /tmp/youtube-nfo


FROM alpine:latest
COPY --from=build /tmp/youtube-nfo /bin/youtube-nfo

WORKDIR /data
VOLUME /data

ENTRYPOINT ["/bin/youtube-nfo"]
